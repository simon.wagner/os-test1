package main

import (
	"fmt"
	"net/http"
	"net"
	"errors"
	"os"
)

func externalIP() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}
	return "", errors.New("are you connected to the network?")
}


func helloHandler(w http.ResponseWriter, r *http.Request) {
    ip, _ := externalIP()
	fmt.Fprintln(w, "<h1>Hello OpenShift! Updated build ["+ ip+ "]  </h1>")
}

func listenAndServe(port string) {
	fmt.Printf("serving on %s\n", port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}

func main() {
	http.HandleFunc("/", helloHandler)
	go listenAndServe("8080")
	select {}
}